package Poker;

import java.util.Collections;

public class Game {
    public Deck gameDeck;
    public Player htb;
    public Player winner;
    public AIP aip;
    public String winningHand;

    public Game() {
        this.gameDeck = new Deck();
        htb = new Player();
        aip = new AIP();
    }

    public Game(String input) {
        this.gameDeck = new Deck(input);
        htb = new Player();
        aip = new AIP();
    }

    //implement tie-breakers and hand checkers
    public Player checkWin(){return null;}

    public void playGame(boolean shuffle, boolean deal){
        //System.out.println("Welcome to the World Famous Underground Poker Tournament");
        if(shuffle){gameDeck.shuffleDeck();}
        if(deal) {deal();}
        System.out.println("HTB's Hand: ");
        htb.hand.printHand();

        System.out.println("\nAIP's Hand: ");
        aip.hand.printHand();

        aip.checkHand();
        aip.printCardsToSwap();
        gameDeck.returnCardToDeck(aip.SwapCards());
        replenishHands();

        System.out.println("\nAIP's Hand (after swap): ");
        aip.hand.printHand();

        winner = resolveWinner();

        System.out.println("\n\t>>> WINNER <<<\n"+winner.name + " is the winner, with a " +winner.hand.getStatus()+":");
        winner.hand.printHand();

        winningHand = winner.hand.toString();

    }

public Player resolveWinner(){
        aip.hand.refreshStatus();
        htb.hand.refreshStatus();
        if(aip.hand.getStatusScore()>htb.hand.getStatusScore()) return aip;
        else if (aip.hand.getStatusScore()<htb.hand.getStatusScore()) return htb;
        else{
            Hand ai_temp = new Hand();
            Hand p_temp = new Hand();

            ai_temp.getCards().addAll(aip.hand.getCards());
            p_temp.getCards().addAll(htb.hand.getCards());
            Collections.sort(ai_temp.getCards());
            Collections.sort(p_temp.getCards());

            if(aip.hand.getStatusScore() == 9){
                if(ai_temp.getCards().get(0).getSuitScore()>p_temp.getCards().get(0).getSuitScore()) return aip;
                else return htb;
            }
            else if(aip.hand.getStatusScore() == 8){
                if(ai_temp.getCards().get(4).getRank()>p_temp.getCards().get(4).getRank()) return aip;
                else if (ai_temp.getCards().get(4).getRank()<p_temp.getCards().get(4).getRank()) return htb;
                else {
                    if (ai_temp.getCards().get(0).getSuitScore() > p_temp.getCards().get(0).getSuitScore()) return aip;
                    else return htb;
                }
            }
            else if(aip.hand.getStatusScore()==7){
                return inspectHands(ai_temp,p_temp,3);
            }
            else if(aip.hand.getStatusScore()==6){
                return inspectHands(ai_temp,p_temp,3);
            }
            else if(aip.hand.getStatusScore()==5){
                return flushResolution(ai_temp,p_temp);
            }
            else if(aip.hand.getStatusScore()==4){
                return inspectStraight(ai_temp,p_temp);
            }
            else if(aip.hand.getStatusScore()==3){
                return inspectHands(ai_temp,p_temp,3);
            }
            else if(aip.hand.getStatusScore()==2){
                return inspectHands(ai_temp,p_temp,4);
            }
            else if(aip.hand.getStatusScore()==1){
                return pairResolution();
            }
            else
                {return highCardResolution(ai_temp,p_temp);}
        }
}

private Player flushResolution(Hand ai_temp, Hand p_temp){
        int mismatchPosition = 0;
        for(int i = 5; i>0; i--)
        {if(ai_temp.getCards().get(i-1).getRank() != p_temp.getCards().get(i-1).getRank())
        {mismatchPosition = i-1; i = 0;}
        }
        if(mismatchPosition == -1)
        {
            if(ai_temp.getCards().get(0).getSuitScore() > p_temp.getCards().get(0).getSuitScore()) return aip;
            else return htb;
        }
        else
        {
            if(ai_temp.getCards().get(mismatchPosition).getRank() > p_temp.getCards().get(mismatchPosition).getRank()) return aip;
            else return htb;
        }

}

private Player highCardResolution(Hand ai_temp, Hand p_temp){
    int aiRank = ai_temp.getCards().get(4).getRank();
    int aiSuit = ai_temp.getCards().get(4).getSuitScore();
    int pRank = p_temp.getCards().get(4).getRank();
    int pSuit = p_temp.getCards().get(4).getSuitScore();

    if(aiRank>pRank) return aip;
    else if(aiRank<pRank) return htb;
    else
    {
        if(aiSuit>pSuit) return aip;
        else return htb;
    }
}

private Player pairResolution(){
    int aiRank = 0;
    int aiSuit = 0;
    int pRank = 0;
    int pSuit = 0;
    Card check = aip.hand.getCards().get(0);
    for (int i = 0; i<aip.hand.getCards().size();i++)
    {
        if(check.getRank()-aip.hand.getCards().get(i).getRank()==0)
        {aiRank = check.getRank();
            aiSuit = greaterOf(check.getSuitScore(),aip.hand.getCards().get(i).getSuitScore());
            break;
        }
        check = aip.hand.getCards().get(i);
    }
    check = htb.hand.getCards().get(0);
    for (int i = 0; i<htb.hand.getCards().size();i++)
    {
        if(check.getRank()-htb.hand.getCards().get(i).getRank()==0)
        {pRank = check.getRank();
        pSuit = greaterOf(check.getSuitScore(),htb.hand.getCards().get(i).getSuitScore());
        break;
        }
        check = htb.hand.getCards().get(i);
    }

    if(aiRank>pRank) return aip;
    else if(aiRank<pRank) return htb;
    else
        {
        if(aiSuit>pSuit) return aip;
        else return htb;
        }
}

private int greaterOf(int a, int b){
        if(a>b) return a;
        else return b;
}

private Player inspectHands(Hand ai_temp,Hand p_temp, int cardNum)
{
    if(ai_temp.getCards().get(cardNum).getRank()>p_temp.getCards().get(cardNum).getRank()) return aip;
    else if (ai_temp.getCards().get(cardNum).getRank()<p_temp.getCards().get(cardNum).getRank()) return htb;
    else {
        if (ai_temp.getCards().get(cardNum).getSuitScore() > p_temp.getCards().get(cardNum).getSuitScore()) return aip;
        else return htb;
    }

}
private Player inspectStraight(Hand ai_temp, Hand p_temp){
        int ai_highCard;
        int p_highCard;
        if(ai_temp.getCards().get(4).getRank()==14 && ai_temp.getCards().get(0).getRank()==2) ai_highCard = 5;
            else ai_highCard = ai_temp.getCards().get(4).getRank();
        if(p_temp.getCards().get(4).getRank()==14 && p_temp.getCards().get(0).getRank()==2) p_highCard = 5;
            else p_highCard = p_temp.getCards().get(4).getRank();

        if(ai_highCard>p_highCard) return aip;
        else if(ai_highCard<p_highCard) return htb;
        else
            {
            if(ai_highCard == 5)
                {
                if(ai_temp.getCards().get(3).getSuitScore() > p_temp.getCards().get(3).getSuitScore()) return aip;
                else return htb;
                }

            else
                {
                if(ai_temp.getCards().get(4).getSuitScore() > p_temp.getCards().get(4).getSuitScore()) return aip;
                else return htb;
                }
            }


}
    private void deal(){
        for(int i = 0; i<5; i++)
        {htb.hand.addCard(gameDeck.takeTopCard());}

        for(int i = 0; i<5; i++)
        {aip.hand.addCard(gameDeck.takeTopCard());}
    }
    private void replenishHands(){
        while(aip.hand.getCards().size()<5)
        {aip.hand.addCard(gameDeck.takeTopCard()); }

        aip.hand.refreshStatus();
        htb.hand.refreshStatus();
    }

   /* public static void main(String[] args) {
        Poker.Game pokerGame = new Poker.Game();
        pokerGame.playGame();

    }*/



}