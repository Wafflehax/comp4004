package Poker;

public class Player {
    public Hand hand;
    protected String name = "HTB";

    public Player() {
        this.hand = new Hand();
    }


    public String getName() {
        return name;
    }

    protected void showHand(){
        System.out.println(name+"'s hand: ");
        for(int i = 0; i<hand.getCards().size();i++)
        {System.out.print(hand.getCards().get(i));}
        System.out.println();
}
}
