package CucumberTesting;
import Poker.*;
import cucumber.annotation.en.*;
import java.util.Collections;
import static org.junit.Assert.*;

public class HandRecognitionDefinitions {
    Hand toTest;

    @Then("^I see \"([^\"]*)\" for each of (\\d+) shuffles$")
    public void iSeeForEachOfShuffles(String expected, int shuffles) throws Throwable {
        doShuffleAndTest(expected,shuffles);
    }

    private void doShuffleAndTest(String expectedStatus, int shuffles) {
        System.out.println();
        for(int i=0; i<shuffles; i++)
        {
            Collections.shuffle(toTest.getCards());

            System.out.println("Shuffle "+(i+1)+":");
            toTest.printHand();

            toTest.refreshStatus();
            assertEquals(expectedStatus,toTest.getStatus());

            System.out.println();
        }
    }

    @Given("^I have a Royal Flush$")
    public void iHaveARoyalFlush() throws Throwable {
        System.out.println("\n\t::Dealing a royal flush::");

        toTest = new Hand();
        toTest.addCard(new Card("HA"));
        toTest.addCard(new Card("HK"));
        toTest.addCard(new Card("HQ"));
        toTest.addCard(new Card("HJ"));
        toTest.addCard(new Card("H10"));
    }

    @Given("^I have a Straight Flush$")
    public void iHaveAStraightFlush() throws Throwable {
        System.out.println("\n\t::Dealing a straight flush::");

        toTest = new Hand();
        toTest.addCard(new Card("S3"));
        toTest.addCard(new Card("S4"));
        toTest.addCard(new Card("S5"));
        toTest.addCard(new Card("S6"));
        toTest.addCard(new Card("S7"));
    }


    @Given("^I have a Four of a kind$")
    public void iHaveAFourOfAKind() throws Throwable {
        System.out.println("\n\t::Dealing a Four of A Kind::");

        toTest = new Hand();
        toTest.addCard(new Card("S3"));
        toTest.addCard(new Card("H3"));
        toTest.addCard(new Card("C3"));
        toTest.addCard(new Card("D3"));
        toTest.addCard(new Card("S7"));
    }

    @Given("^I have a Full House$")
    public void iHaveAFullHouse() throws Throwable {
        System.out.println("\n\t::Dealing a Full House::");

        toTest = new Hand();
        toTest.addCard(new Card("S3"));
        toTest.addCard(new Card("H3"));
        toTest.addCard(new Card("C3"));
        toTest.addCard(new Card("D7"));
        toTest.addCard(new Card("S7"));
    }

    @Given("^I have a Flush$")
    public void iHaveAFlush() throws Throwable {
        System.out.println("\n\t::Dealing a Full House::");

        toTest = new Hand();
        toTest.addCard(new Card("S3"));
        toTest.addCard(new Card("S5"));
        toTest.addCard(new Card("S8"));
        toTest.addCard(new Card("S9"));
        toTest.addCard(new Card("SK"));
    }

    @Given("^I have a Straight$")
    public void iHaveAStraight() throws Throwable {
        System.out.println("\n\t::Dealing a Full House::");

        toTest = new Hand();
        toTest.addCard(new Card("C5"));
        toTest.addCard(new Card("C6"));
        toTest.addCard(new Card("H7"));
        toTest.addCard(new Card("S8"));
        toTest.addCard(new Card("D9"));
    }


    @Given("^I have a Three of a kind$")
    public void iHaveAThreeOfAKind() throws Throwable {
        System.out.println("\n\t::Dealing a Full House::");

        toTest = new Hand();
        toTest.addCard(new Card("SK"));
        toTest.addCard(new Card("HK"));
        toTest.addCard(new Card("DK"));
        toTest.addCard(new Card("S5"));
        toTest.addCard(new Card("H2"));
    }

    @Given("^I have Two Pairs$")
    public void iHaveTwoPairs() throws Throwable {
        System.out.println("\n\t::Dealing a Full House::");

        toTest = new Hand();
        toTest.addCard(new Card("D10"));
        toTest.addCard(new Card("S10"));
        toTest.addCard(new Card("H5"));
        toTest.addCard(new Card("C5"));
        toTest.addCard(new Card("D9"));
    }

    @Given("^I have a Pair$")
    public void iHaveAPair() throws Throwable {
        System.out.println("\n\t::Dealing a Full House::");

        toTest = new Hand();
        toTest.addCard(new Card("CQ"));
        toTest.addCard(new Card("HQ"));
        toTest.addCard(new Card("S8"));
        toTest.addCard(new Card("C3"));
        toTest.addCard(new Card("DA"));
    }
}
