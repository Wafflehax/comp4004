Feature: Hand Recognition

  #in each implementation, I run a for-loop to test for 20 random permutations of each hand using Collections.shuffle(toTest.getCards())
  Scenario: Validate Royal Flush
    Given I have a Royal Flush
    Then I see "royalflush" for each of 20 shuffles

  Scenario: Validate Straight Flush
    Given I have a Straight Flush
    Then I see "straightflush" for each of 20 shuffles

  Scenario: Validate Four of a Kind
    Given I have a Four of a kind
    Then I see "4ofakind" for each of 20 shuffles

  Scenario: Validate Full House
    Given I have a Full House
    Then I see "fullhouse" for each of 20 shuffles

  Scenario: Validate Flush
    Given I have a Flush
    Then I see "flush" for each of 20 shuffles

  Scenario: Validate Straight
    Given I have a Straight
    Then I see "straight" for each of 20 shuffles

  Scenario: Validate Three of a Kind
    Given I have a Three of a kind
    Then I see "3ofakind" for each of 20 shuffles

  Scenario: Validate Two Pair
    Given I have Two Pairs
    Then I see "2pair" for each of 20 shuffles

  Scenario: Validate Pair
    Given I have a Pair
    Then I see "1pair" for each of 20 shuffles